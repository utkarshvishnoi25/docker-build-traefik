FROM traefik:latest

ARG ROOT_CERTIFICATE_LINK
ENV ROOT_CERTIFICATE_LINK=${ROOT_CERTIFICATE_LINK}

RUN apk add --no-cache curl jq

COPY entrypoint.sh /
RUN chmod +x entrypoint.sh