# Traefik Custom Docker Image

A custom docker image with support to fetch and install a self signed root certificate.

## Build

```bash
  git clone https://gitlab.com/utkarshvishnoi25/docker-build-traefik.git
  cd docker-build-traefik
  docker build --build-arg ROOT_CERTIFICATE_LINK=<Certificate Link> . -t utkarsh/traefik
```

## Authors

- [@utkarsh-vishnoi](https://www.github.com/utkarsh-vishnoi)
