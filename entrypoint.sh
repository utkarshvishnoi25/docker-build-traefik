#!/bin/sh
set -e

CONTAINER_ALREADY_STARTED="CONTAINER_ALREADY_STARTED_PLACEHOLDER"
if [ ! -e $CONTAINER_ALREADY_STARTED ]; then
    touch $CONTAINER_ALREADY_STARTED
    echo "-- First container startup --"
    wget $ROOT_CERTIFICATE_LINK -q -O - --no-check-certificate | jq -r .crts[0] >/usr/local/share/ca-certificates/root.crt
    sed -i '$ d' /usr/local/share/ca-certificates/root.crt
    update-ca-certificates
else
    echo "-- Not first container startup --"
fi

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
    set -- traefik "$@"
fi

# if our command is a valid Traefik subcommand, let's invoke it through Traefik instead
# (this allows for "docker run traefik version", etc)
if traefik "$1" --help >/dev/null 2>&1; then
    set -- traefik "$@"
else
    echo "= '$1' is not a Traefik command: assuming shell execution." 1>&2
fi

exec "$@"
